/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "basetype.h"
#include "trace.h"
#include "config.h"
#include "board.h"
#include "uart.h"
#include "gpio.h"
#include "mytimer.h"
#include "system.h"
#include "pt.h"

typedef enum{
    KEY_ID_K1=0,
    KEY_ID_K2,
    KEY_ID_K3,
    KEY_ID_K4,
    KEY_ID_MAX
};
static uint8 key_get_state(uint8 keyID)
{
    switch(keyID){
    case KEY_ID_K1:
        return GPIO_GET_VAL(KEY_K1);
    case KEY_ID_K2:
        return GPIO_GET_VAL(KEY_K2);
    case KEY_ID_K3:
        return GPIO_GET_VAL(KEY_K3);
    case KEY_ID_K4:
        return GPIO_GET_VAL(KEY_K4);
    default:
        return 0xFF;
    }
}

static TickTimer_S s_key_timer; 
static uint8 s_k1_state;
PT_THREAD(thread_input_k1(struct pt* pt))
{
    PT_BEGIN(pt);
    if(key_get_state(KEY_ID_K1)!=s_k1_state)/* 状态发生了改变 */
    {
        ticktimer_set(&s_key_timer, 2);
        PT_WAIT_UNTIL(pt, ticktimer_expired(&s_key_timer));
        if(key_get_state(KEY_ID_K1)!=s_k1_state)
        {
            if(s_k1_state==1)
            {
                TRACE_DBG("K1 pressed!"); 
            }
            else
            {
                TRACE_DBG("K1 release!"); 
            }
            s_k1_state = !s_k1_state;
        }
    }
    PT_END(pt);
}

static uint8 s_k2_state;
PT_THREAD(thread_input_k2(struct pt* pt))
{
    PT_BEGIN(pt);
    if(key_get_state(KEY_ID_K2)!=s_k2_state)/* 状态发生了改变 */
    {
        ticktimer_set(&s_key_timer, 2);
        PT_WAIT_UNTIL(pt, ticktimer_expired(&s_key_timer));
        if(key_get_state(KEY_ID_K2)!=s_k2_state)
        {
            if(s_k2_state==1)
            {
                TRACE_DBG("K2 pressed!"); 
            }
            else
            {
                TRACE_DBG("K2 release!"); 
            }
            s_k2_state = !s_k2_state;
        }
    }
    PT_END(pt);
}

static void shell_handle(void)
{ 
    char* argv[1]={0};
    if(system_shell_read(UART_COM0,argv,1,0)>0)
    {
        if(strcmp(argv[0],"quit")==0)
        {
            TRACE_DBG("do something for quit.");
        }
    }
}

static void on_led_timer(void)
{
    if(GPIO_GET_VAL(LED_L7)==0)
    {
        GPIO_SET_VAL(LED_L7,1);
    }
    else
    {
        GPIO_SET_VAL(LED_L7,0);
    }
}

/*****************************************************
                Main 主程序
 *****************************************************/
void main()
{
    sint8 ledTimer;
    struct pt k1_pt,k2_pt;
    
    PT_INIT(&k1_pt);
    PT_INIT(&k2_pt);

    platform_init();
    board_init();
    uart_init(COM_DEBUG);
    ledTimer = timer_create(TIMER_TYPE_PERIODIC, 500, (SEL_ontimer)(on_led_timer));
    timer_start(ledTimer);

    s_k1_state = key_get_state(KEY_ID_K1);
    s_k2_state = key_get_state(KEY_ID_K2);
    while(1)
    {
        thread_input_k1(&k1_pt);
        thread_input_k2(&k2_pt);
        shell_handle();
    }
}

