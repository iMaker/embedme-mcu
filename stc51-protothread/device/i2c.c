/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "gpio.h"
#include "trace.h"
#include "i2c.h"

#define TIME_KP             5               /* I2C保持时间 */
#define TIME_WAIT_ACK       250             /* 等待回应超时时间 */
#define I2C_ADDR_RD(dev)    ((dev)|0x01)    /* 读时地址 */
#define I2C_ADDR_WR(dev)    ((dev)&0xFE)    /* 写时地址 */

static void i2c_set_sda(uint8 i2c_bus,uint8 val)
{
    GPIO_SET_VAL(I2C_SDA,val);
}

static void i2c_set_scl(uint8 i2c_bus,uint8 val)
{
    GPIO_SET_VAL(I2C_SCL,val);
}

static uint8 i2c_get_databit(uint8 i2c_bus)
{
    return GPIO_GET_VAL(I2C_SDA);
}


/* I2C开始信号:SCL高电平期间,SDA由高变低 */
static void i2c_start(uint8 i2c_bus)
{
    i2c_set_sda(i2c_bus,1);
    udelay(TIME_KP);
    i2c_set_scl(i2c_bus,1);
    udelay(TIME_KP);
    i2c_set_sda(i2c_bus,0);
    udelay(TIME_KP);
}

/* I2C停止信号:SCL高电平期间,SDA由低变高 */
static void i2c_stop(uint8 i2c_bus)
{
    i2c_set_sda(i2c_bus,0);
    udelay(TIME_KP);
    i2c_set_scl(i2c_bus,1);
    udelay(TIME_KP);
    i2c_set_sda(i2c_bus,1);
    udelay(TIME_KP);
}

/* I2C主机应答信号:SDA为低电平,表示主机接收字节完毕 */
static void i2c_ack(uint8 i2c_bus)
{
    i2c_set_sda(i2c_bus,0);
	udelay(TIME_KP);
	i2c_set_scl(i2c_bus,1);
	udelay(TIME_KP);
	i2c_set_scl(i2c_bus,0);
	udelay(TIME_KP);
}

/* I2C主机非应答信号:SDA为高电平,表示主机结束接收从机数据 */
static void i2c_nack(uint8 i2c_bus)
{
    i2c_set_sda(i2c_bus,1);
	udelay(TIME_KP);
	i2c_set_scl(i2c_bus,1);
	udelay(TIME_KP);
	i2c_set_scl(i2c_bus,0);
	udelay(TIME_KP);
}

/* I2C主机等待ACK信号:等待SDA线被拉低 */
static void i2c_wait_ack(uint8 i2c_bus)
{
    uint8 timeout = TIME_WAIT_ACK;
    i2c_set_scl(i2c_bus,1);
    udelay(TIME_KP);
	while(timeout--)
	{
        if(i2c_get_databit(i2c_bus)==0)
        {
            i2c_set_scl(i2c_bus,0);
            udelay(TIME_KP);
            return;
        }
	}
	i2c_set_scl(i2c_bus,0);
    udelay(TIME_KP);
	TRACE_ERR("i2c write timeout.\r\n");
}

static uint8 i2c_read_byte(uint8 i2c_bus)
{
    uint8 i;
    uint8 ret=0;
    i2c_set_scl(i2c_bus,0);/* 读前拉低SCL,为产生时序做准备 */
    udelay(TIME_KP);
    i2c_set_sda(i2c_bus,1);
    udelay(TIME_KP);
    for(i=0;i<8;i++)
    {
        i2c_set_scl(i2c_bus,1);
        udelay(TIME_KP);
        ret = (ret<<1) | i2c_get_databit(i2c_bus);
        i2c_set_scl(i2c_bus,0);/* 读完后拉低SCL */
        udelay(TIME_KP);    
    }
    return ret;
}

static void i2c_write_byte(uint8 i2c_bus, uint8 byte)
{
    uint8 i;
    for(i=0; i<8; i++)
    {
        i2c_set_scl(i2c_bus,0);/* 写前拉低SCL,为产生时序做准备 */
        udelay(TIME_KP);
        if ((byte<<i)&0x80)
        {
            i2c_set_sda(i2c_bus,1);
        }
        else
        {
            i2c_set_sda(i2c_bus,0);
        }
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,1);
        udelay(TIME_KP);
    }
    i2c_set_scl(i2c_bus,0);/* 写完后拉低SCL */
	udelay(TIME_KP);
	i2c_set_sda(i2c_bus,1);  /* 写完后拉高SDA,因为后续要等待ACK信号 */
	udelay(TIME_KP);
}

bool i2c_gpio_init(uint8 i2c_bus, uint8 i2c_speed)
{
    UNUSED_PARAM(i2c_bus);
    UNUSED_PARAM(i2c_speed);
    i2c_set_sda(i2c_bus,0);
    udelay(TIME_KP);
    i2c_set_scl(i2c_bus,0);
    udelay(TIME_KP);
	return true;
}
uint8 i2c_gpio_read(uint8 i2c_bus, uint8 i2c_dev, uint8* wdata, uint8 wlen, uint8* rdata,  uint8 rlen)
{
    uint8 i=0;
    UNUSED_PARAM(i2c_bus);
    if (wlen>0)/* 要先写数据 */
    {
        i2c_start();
        i2c_write_byte(I2C_ADDR_WR(i2c_dev));/* 写地址 */
        i2c_wait_ack();
        for(i=0;i<wlen;i++)
        {
            i2c_write_byte(*(wdata+i));
            i2c_wait_ack();
        }
    }

    i2c_start();
    i2c_write_byte(I2C_ADDR_RD(i2c_dev));
    i2c_wait_ack();
    i=0;
    while(rlen>0)
    {
        rdata[i++]=i2c_read_byte();
        if (rlen==1)
        {
            i2c_nack();
        }
        else
        {
            i2c_ack();
        }
        rlen--;
    }
    i2c_stop();
    return i;
}
uint8 i2c_gpio_write(uint8 i2c_bus, uint8 i2c_dev, uint8 * wdata, uint8 len)
{
    uint8 i=0;
    UNUSED_PARAM(i2c_bus);
    if(len>0)
    {
		i2c_start();
	    i2c_write_byte(I2C_ADDR_WR(i2c_dev));
	    i2c_wait_ack();
	    for(i=0;i<len;i++)
	    {
	        i2c_write_byte(*(wdata+i));
	        i2c_wait_ack();
	    }
	    i2c_stop();
	}
    return i;
}

