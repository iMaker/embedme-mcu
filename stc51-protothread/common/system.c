/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "system.h"
#include "uart.h"
#include <string.h>

#define SHELL_CMD_LEN_MAX   16  /* shell命令最长为16字节 */
static char s_shellChBuf[SHELL_CMD_LEN_MAX];
static uint8 s_shellChIdx=0;
static void system_shell_reset(void)
{
    s_shellChIdx = 0;
    memset(s_shellChBuf,0,SHELL_CMD_LEN_MAX);
}

/*******************************************************
 * shell_handle_cmd:
 * 解析命令参数
 ******************************************************/
static uint8 system_shell_parse(char* cmd,char** argv,uint8 argvnum)
{
    uint8 i=0;
    uint8 start=0;
    uint8 argc=0;
    sint8 index=-1;
    uint8 cmdLen = strlen(cmd);
    for(i=0; i<cmdLen; i++)
    {
        if(cmd[i]==' ')
        {
            cmd[i]=0;
            if(i-start>0)
            {
                index++;
                if(index>=argvnum)
                    break;
                else
                    argv[index]=cmd+start;   
            }
            start=i+1;
        } 
    }
    /* 到达缓冲尾部 */
    if(start<i)
    {
        index++;
        if(index<argvnum)
            argv[index]=cmd+start; 
    }
    /* 计算参数个数 */
    for(i=0;i<argvnum;i++)
    {
        if(argv[i]!=NULL)
            argc++;
    }
    return argc;
}


/* 从终端获取用户输入 */
uint8 system_shell_read(uint8 uart, char** argv, uint8 argvnum,char ps1)
{
    uint8 argc=0;
    char* shellCmd = s_shellChBuf;
    if(s_shellChIdx==0)
    {
        if (ps1!=0)
        {
            uart_send_byte(uart, ps1);
        }
        system_shell_reset();
    }
    while(1)
    {
        char ch=0;
        if(uart_recv_byte(uart, &ch)>0)
        {
            uart_send_byte(uart,ch);
            if(s_shellChIdx<SHELL_CMD_LEN_MAX)
                shellCmd[s_shellChIdx++]=ch;
            else
                ch = '\n';/* 到达最大长度,自动结束!!! */
            if(ch=='\r' || ch=='\n')/* 终端换行符可能只发一个'\r',不会发'\n' */
            {
                shellCmd[s_shellChIdx-1]=0;
                if(strlen(shellCmd)>0)
                {
                    uart_send_byte(uart,'\r');
                    uart_send_byte(uart,'\n');
                    argc = system_shell_parse(shellCmd,argv,argvnum);
                    s_shellChIdx = 0;
                    return argc;
                }
                else/* strlen(shellCmd)==0,只包含一个'\r'或'\n'的情况下,继续等待有效输入 */
                {
                    uart_send_byte(uart,'\r');
                    uart_send_byte(uart,'\n');
                    s_shellChIdx = 0;
                    return 0;
                }
            }
        }
        else
        {
            return 0;
        }
    }
}

