/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
/******************************************************************************
* @file     :   mytimer.c
* @bref     :   software timer implementation
* @version  :   v.1.0
* @date     :   2013-12-12
* @author   :   FergusZeng
* @note     :   软件定时器
                最多可创建128个定时器(可根据mcu资源进行配置)
                最小精度为100ms
                最长定时为3276800ms
******************************************************************************/
#include <stdio.h>
#include "platform.h"
#include "mytimer.h"

/*-------------------------------------------------------------------------
* 如果系统定时器的时钟片周期我们设置为100ms的定时精度,
* 则最长定时时间则为2^16*100ms=6553600ms=6553s
-------------------------------------------------------------------------*/
typedef struct{
    uint8 m_isused:1;   /* 0:未使用,1:已使用 */
    uint8 m_isstart:1;  /* 0:停止,1:开始 */
    uint8 m_timertype:4;/* 定时器类型 */
    uint16 m_timeout;   /* 定时时间片数 */
    SEL_ontimer m_ontimer;
}Timer_S;
static Timer_S s_timerpool[TIMER_COUNT]={0};
static uint16 s_timerticks=0;

sint8 timer_create(uint8 timerType,uint16 ms,SEL_ontimer ontimer)
{
    uint8 i;
    for (i=0;i<TIMER_COUNT; i++)
    {
        if (s_timerpool[i].m_isused==0)
        {
            s_timerpool[i].m_isused=true;
            s_timerpool[i].m_isstart=false;
            s_timerpool[i].m_timertype = timerType;
            s_timerpool[i].m_timeout = ms/(SYSTEM_TICK_MS);
            s_timerpool[i].m_ontimer = ontimer;
            return i;
        }
    }
    return -1;
}

void timer_destroy(sint8 timerID)
{
    if (timerID>=0 &&
        timerID<TIMER_COUNT &&
        (s_timerpool[timerID].m_isused==1))
    {
        s_timerpool[timerID].m_isused=0;
    }
}

bool timer_start(sint8 timerID)
{
    if (timerID>=0 &&
        timerID<TIMER_COUNT&&
        (s_timerpool[timerID].m_isused==1) &&
        (s_timerpool[timerID].m_ontimer!=0))
    {
        s_timerpool[timerID].m_isstart=true;
        return true;
    }
    return false;
}

bool timer_stop(sint8 timerID)
{
    if (timerID>=0 &&
        timerID<TIMER_COUNT &&
        (s_timerpool[timerID].m_isused==1))
    {
        s_timerpool[timerID].m_isstart=false;
        return true;
    }
    return false;
}

/* 该函数用于给系统定时器回调,回调频率为硬件定时器中断频率,以此作为事件标准 */
void system_time_tick(void)
{
    uint8 i=0;
    /* 每隔SYSTEM_TICKS_MS执行一次 */
    for(i=0;i<TIMER_COUNT;i++)
    {
        if(s_timerpool[i].m_isstart)
        {
            if((++s_timerticks)%(s_timerpool[i].m_timeout)==0)
            {
                if (s_timerpool[i].m_ontimer!=NULL)
                {
                   s_timerpool[i].m_ontimer(); 
                }
                if(TIMER_TYPE_ONESHOT==s_timerpool[i].m_timertype)
                {
                    s_timerpool[i].m_isused=false;
                }
            }
        }
    }    
}

