/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __SYSTEM_H__
#define __SYSTEM_H__
#include "basetype.h"
#include "kernel.h"
typedef struct{
    uint8 m_second;     /* ȡֵ: 0~59 */
    uint8 m_minute;     /* ȡֵ: 0~59 */
    uint16 m_hour:5;    /* ȡֵ: 0~23 */
    uint16 m_date:5;    /* ȡֵ: 1~31 */
    uint16 m_weekday:6; /* ȡֵ: 0~6 */
    uint16 m_month:4;   /* ȡֵ: 1~12 */
    uint16 m_year:12;   /* ȡֵ: 0~4095 */
}Time_S;

uint32 ms2ticks(uint32 ms); /* msת����ticks */
void msleep(uint32 ms);     /* ����OSTimeDlyʵ�ֵ���ʱ,˯��ʱ����������л� */
void sleep(uint32 sec);     /* ����OSTimeDlyʵ�ֵ���ʱ,˯��ʱ����������л� */
void system_reboot(void);
void system_frozen(char* reason);
uint8 system_shell_read(uint8 uart, char** argv, uint8 argvnum, char ps1);
void system_task_info(uint8 taskID, char* taskName);
void system_core_dump(OS_TID taskID);

#endif

