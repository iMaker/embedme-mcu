/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include "basetype.h"
#include "stm32f10x.h"

#define MAIN_OSC_FRQ        8000000L
#define RTC_OSC_FRQ         32768L

#define CPU_MIPS            4000000L
#define CPU_MIPMS           4000L
#define CPU_MIPUS           4L


/* 平台初始化函数,初始化系统时钟,中断等 */
void platform_init(void);
void stm32_sysclk_init(uint32 hclkdiv,uint32 pclk2div,uint32 pclk1div,uint32 pllsrc,uint32 pllmul);
uint32 stm32_sysclk_get(void);
void stm32_exti_init(uint32 extiline,uint8 mode,uint8 trigger,uint8 portsrc,uint8 pinsrc);
void stm32_nvic_init(uint32 prioritygroup,uint8 irqn,uint8 preemption,uint8 sub);
void stm32_usart_init(uint32 usart,uint32 baud,uint16 databits, uint16 stopbits,uint16 parity);
void stm32_power_standby(void);
void mdelay(uint32 ms);/* 忙等待延时(毫秒级) */
void udelay(uint32 us);/* 忙等待延时(微秒级) */

#endif

