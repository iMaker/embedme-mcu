/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "trace.h"
#include <stdarg.h>

#if USE_TRACE
char g_traceBuf[TRACE_BUF_LEN];
sint8 g_traceLevel=TRACE_LEVEL_DIS;

#if DEBUG_VERSION
void mcu_assert_failed(char * file, uint32 line)
{
    TRACE_ERR("ASSERT FAILED:%s,L%d\n",file,line);
    while(1){};
}
#endif

#if !(USE_STD_PRINTF)
static void mcu_print_value(sint32 value,uint8 radix)
{
    char s[16]={0};
    if (radix==16)
    {
        snprintf(s,15,"%x",value);
    }
    else
    {
        snprintf(s,15,"%d",value);
    }
    PRINTF("%s",s);
}

int mcu_printf(const char* fmt,...)
{
    int d;
    char* s;
    char* ptr;
    va_list args;
    va_start(args, fmt);
    ptr=(char*)fmt;
    while(*ptr!=0)
    {
        if(*ptr=='%' && *(ptr+1)=='d')
        {
            d=va_arg(args,int);
            mcu_print_value(d,10);
            ptr+=2;
            continue;
        }
        else if (*ptr=='%' && *(ptr+1)=='x')
        {
            d=va_arg(args,int);
            mcu_print_value(d,16);
            ptr+=2;
            continue;
        }
        else if (*ptr=='%' && *(ptr+1)=='s')
        {
            s=va_arg(args,char*);
            PRINTF("%s",s);
            PRINTF("%s",ptr+2);
            ptr+=2;
            continue;
        }
        else
        {
            char str[2];
            str[0]= *ptr;
            str[1]=0;
            PRINTF("%s",str);
        }
        ptr++;
    }
    va_end(args);
    return 0;
}
#endif
#endif
