/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __I2C_H__
#define __I2C_H__
#include "basetype.h"

#define USE_I2C_BASED_ON_GPIO    1   /* �Ƿ�ʹ��GPIOģ��i2c */

typedef enum{
    I2C_BUS_0=0,
    I2C_BUS_1,
    I2C_BUS_MAX
}I2C_BUS_E;

typedef enum{
    I2C_SPEED_100K=100000,
    I2C_SPEED_400K=400000,
}I2C_SPEED_E;

#if USE_I2C_BASED_ON_GPIO
#define I2C_INIT(i2c_bus,i2c_speed) i2c_gpio_init(i2c_bus, i2c_speed)
#define I2C_READ(i2c_bus,i2c_dev,wbuf,wlen,rbuf,rlen) i2c_gpio_read(i2c_bus,i2c_dev,wbuf,wlen,rbuf,rlen)
#define I2C_WRITE(i2c_bus,i2c_dev,wbuf,wlen)  i2c_gpio_write(i2c_bus, i2c_dev, wbuf, wlen)
bool i2c_gpio_init(uint8 i2c_bus, uint8 i2c_speed);
uint32 i2c_gpio_read(uint8 i2c_bus, uint8 i2c_dev, uint8* wdata, uint32 wlen, uint8* rdata,  uint32 rlen);
uint32 i2c_gpio_write(uint8 i2c_bus, uint8 i2c_dev, uint8 * data, uint32 len);

#else
#define I2C_INIT(i2c_bus,i2c_speed) i2c_soc_init(i2c_bus, i2c_speed)
#define I2C_READ(i2c_bus,i2c_dev,wbuf,wlen,rbuf,rlen) i2c_soc_read(i2c_bus,i2c_dev,wbuf,wlen,rbuf,rlen)
#define I2C_WRITE(i2c_bus,i2c_dev,wbuf,wlen)  i2c_soc_write(i2c_bus, i2c_dev, wbuf, wlen)
bool i2c_soc_init(uint8 i2c_bus, uint8 i2c_speed);
uint32 i2c_soc_read(uint8 i2c_bus, uint8 i2c_dev, uint8* wdata, uint32 wlen, uint8* rdata,  uint32 rlen);
uint32 i2c_soc_write(uint8 i2c_bus, uint8 i2c_dev, uint8 * data, uint32 len);
#endif

#endif

