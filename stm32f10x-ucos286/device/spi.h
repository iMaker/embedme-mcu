/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __SPI_H__
#define __SPI_H__

#include "basetype.h"

#define USE_SPI_BASED_ON_GPIO   1   /* 使用GPIO模拟SPI */

typedef enum{
    SPI_DEV_PGA2311_1=0,
    SPI_DEV_PGA2311_2,
    SPI_DEV_AD5160,
    SPI_DEV_MAX,
    SPI_DEV_NA=0xFF
}SPI_DEV_E;

/******************************************************************************
 * SPI模块使用注意事项:
 * 为保证SPI总线在同一时刻只能由一个设备占用,模块中使用了互斥锁(mutex).我们在
 * 使用该模块时要先上锁(调用SPI_OPEN),使用完毕后释放锁(调用SPI_CLOSE).
 *****************************************************************************/
#if USE_SPI_BASED_ON_GPIO
#define SPI_OPEN(spi_dev)    spi_gpio_open(spi_dev)
#define SPI_CLOSE(spi_dev)         spi_gpio_close(spi_dev)
#define SPI_WRITE(spi_dev,byte)       spi_gpio_write(spi_dev,byte)
#define SPI_READ(spi_dev,pByte)       spi_gpio_read(spi_dev,pByte)
#define SPI_XFER_START(spi_dev)    spi_gpio_xfer_start(spi_dev)
#define SPI_XFER_STOP(spi_dev)     spi_gpio_xfer_stop(spi_dev)
bool spi_gpio_open(uint8 spi_dev);
bool spi_gpio_close(uint8 spi_dev);
uint8 spi_gpio_write(uint8 spi_dev,uint8 byte);
uint8 spi_gpio_read(uint8 spi_dev,uint8* pByte);
void spi_gpio_xfer_start(uint8 spi_dev);
void spi_gpio_xfer_stop(uint8 spi_dev);

#else
#define SPI_OPEN(spi_dev)    spi_soc_open(spi_dev)
#define SPI_CLOSE(spi_dev)         spi_soc_close(spi_dev)
#define SPI_WRITE(spi_dev,byte)       spi_soc_write(spi_dev,byte)
#define SPI_READ(spi_dev,pByte)       spi_soc_read(spi_dev,pByte)
#define SPI_XFER_START(spi_dev)    spi_soc_xfer_start(spi_dev)
#define SPI_XFER_STOP(spi_dev)     spi_soc_xfer_stop(spi_dev)
bool spi_soc_open(uint8 spi_dev);
bool spi_soc_close(uint8 spi_dev);
uint8 spi_soc_write(uint8 spi_dev,uint8 byte);
uint8 spi_soc_read(uint8 spi_dev,uint8* pByte);
void spi_soc_xfer_start(uint8 spi_dev);
void spi_soc_xfer_stop(uint8 spi_dev);
#endif

#endif

